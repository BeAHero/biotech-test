export class CookieListItem {
	id?:string;
	statistical?: boolean;
    marketing?: boolean;
    other?: boolean;
}
