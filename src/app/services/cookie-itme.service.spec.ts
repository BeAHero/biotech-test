import { TestBed } from '@angular/core/testing';

import { CookieItmeService } from './cookie-itme.service';

describe('CookieItmeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CookieItmeService = TestBed.get(CookieItmeService);
    expect(service).toBeTruthy();
  });
});
