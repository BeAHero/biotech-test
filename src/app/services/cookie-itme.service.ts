import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { CookieListItem } from '../models/cookie-list-item.model';
import { Observable, of, from } from 'rxjs';

import 'rxjs/add/operator/map'

@Injectable({
  providedIn: 'root'
})
export class CookieItmeService {
	cookieItemsCollection: AngularFirestoreCollection<CookieListItem>;

	cookieItems: Observable<CookieListItem[]>;

  constructor(public afs: AngularFirestore) { 
  	// this.cookieItems = this.afs.collection('cookies').valueChanges();

    this.cookieItemsCollection = this.afs.collection('cookies');

  	this.cookieItems = this.cookieItemsCollection.snapshotChanges().map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data() as CookieListItem;
        data.id = a.payload.doc.id;
        return data;
      });
    });
 
  }

  getCookieItems(){
  	return this.cookieItems;
  }

  addCookie(cookieItem) {
    this.cookieItemsCollection.add(cookieItem));
  }
}