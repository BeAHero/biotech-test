export class CookieDescriptionHide {
	required: boolean = false;
	statistical: boolean = false;
    marketing: boolean = false;
    other: boolean = false;
}
