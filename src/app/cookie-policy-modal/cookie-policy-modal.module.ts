import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CookiePolicyModalComponent } from './cookie-policy-modal.component';

import { StorageServiceModule } from 'angular-webstorage-service';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';
import { InputSwitchModule } from 'primeng/inputswitch';
import { TableModule } from 'primeng/table';

import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
  	CookiePolicyModalComponent,
  ],
  imports: [
    FormsModule,
    CommonModule,
    DialogModule,
    ButtonModule,
    InputSwitchModule,
    TableModule,
    AngularFontAwesomeModule,
    StorageServiceModule
  ],
  exports: [
  	CookiePolicyModalComponent
  ]
})
export class CookiePolicyModalModule { }
