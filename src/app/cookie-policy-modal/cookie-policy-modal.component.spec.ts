import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CookiePolicyModalComponent } from './cookie-policy-modal.component';

describe('CookiePolicyModalComponent', () => {
  let component: CookiePolicyModalComponent;
  let fixture: ComponentFixture<CookiePolicyModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CookiePolicyModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CookiePolicyModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
