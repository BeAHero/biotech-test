import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms'
import { CookieDescriptionHide } from './shared/cookie-description-hide.obj';
import { TableModule } from 'primeng/table';
import { Inject, Injectable } from '@angular/core';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';

import { CookieItmeService } from '../services/cookie-itme.service';
import { CookieListItem } from '../models/cookie-list-item.model';

@Component({
  selector: 'app-cookie-policy-modal',
  templateUrl: './cookie-policy-modal.component.html',
  styleUrls: ['./cookie-policy-modal.component.css'],
  providers: []
})
export class CookiePolicyModalComponent implements OnInit {

  acceptCookieModal: boolean;
  setUserCookieDatailsModal: boolean = false;

  CookieDescriptionHide = new CookieDescriptionHide();

  userWantAllTheCookies: boolean = true;

  cookieDetailTableContetns;

  cookieItem: CookieListItem ={
      statistical: true,
      marketing: true,
      other: true,
  }

  constructor(private cookieItmeService: CookieItmeService ) {
    let neededModal = localStorage.getItem("neededModal");
     if (neededModal) {
         this.acceptCookieModal = false;
    }else {
        this.acceptCookieModal = true;
    }
  }

	ngOnInit() {
    this.cookieDetailTableContetns = [
            { name: 'Lorem ipsum', description: 'Duis aute irure', expirationDate: '2081.11.12' },
            { name: 'Ut enim ad minim', description: 'Excepteur sint', expirationDate: '2041.20.04' },
        ];
	}

  userWantsAllCookisModalSave() {
    this.cookieItem.statistical = true;
    this.cookieItem.marketing = true;
    this.cookieItem.other = true;

    this.acceptCookieModal = false;
    this.saveAcceptedCookieList();
  }

   openCookieDetailSettingsModal() {
    this.acceptCookieModal = false;
    this.setUserCookieDatailsModal = true;
  }

  userWantsSpecilaCookisModalSave() {
    this.setUserCookieDatailsModal = false;
    this.saveAcceptedCookieList();
  }

  saveAcceptedCookieList() {
        localStorage.setItem("neededModal", "false");
        this.cookieItmeService.addCookie(this.cookieItem);
    }

  handleChange(e) {
        if (!this.cookieItem.statistical || !this.cookieItem.marketing || !this.cookieItem.other)
            this.userWantAllTheCookies = false;
        else
            this.userWantAllTheCookies = true;
    }

    wantAllTheCookieHandleChange(e) {
        if (!this.userWantAllTheCookies) {
            this.cookieItem.statistical = false;
            this.cookieItem.marketing = false;
            this.cookieItem.other = false;
        } else {
            this.cookieItem.statistical = true;
            this.cookieItem.marketing = true;
            this.cookieItem.other = true;
        }
    }

    toggler(event) {
       switch (event.target.id) {
            case 'required-toggle':
            case 'required-toggl-arrow':
                  this.CookieDescriptionHide.other = false;
                  this.CookieDescriptionHide.statistical = false;
                  this.CookieDescriptionHide.marketing = false;
                break;
            case 'statistical-toggle':
            case 'statistical-toggl-arrow':
                  this.CookieDescriptionHide.required = false;
                  this.CookieDescriptionHide.marketing = false;
                  this.CookieDescriptionHide.other = false;
                break;
            case 'marketing-toggle':
            case 'marketing-toggl-arrow':
                  this.CookieDescriptionHide.required = false;
                  this.CookieDescriptionHide.statistical = false;
                  this.CookieDescriptionHide.other = false;
                break;
            case 'other-toggle':
            case 'other-toggl-arrow':
                  this.CookieDescriptionHide.required = false;
                  this.CookieDescriptionHide.statistical = false;
                  this.CookieDescriptionHide.marketing = false;
                break;
            default:
    
                break;
    }
  }

  onSubmiter(){
    alert();
  }
}