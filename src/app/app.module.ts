import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CookiePolicyModalModule } from './cookie-policy-modal/cookie-policy-modal.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FormsModule } from '@angular/forms';

import { environment } from './../environments/environment';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { CookieItemsComponent } from './cookie-items/cookie-items.component';

import { CookieItmeService } from './services/cookie-itme.service';


@NgModule({
  declarations: [
    AppComponent,
    CookieItemsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CookiePolicyModalModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase, 'biotech-test'),
    AngularFirestoreModule,
    FormsModule
  ],
  providers: [
    CookieItmeService
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
