import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CookieItemsComponent } from './cookie-items.component';

describe('CookieItemsComponent', () => {
  let component: CookieItemsComponent;
  let fixture: ComponentFixture<CookieItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CookieItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CookieItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
