import { Component, OnInit } from '@angular/core';
import { CookieItmeService } from '../services/cookie-itme.service';
import { CookieListItem } from '../models/cookie-list-item.model';

@Component({
  selector: 'app-cookie-items',
  templateUrl: './cookie-items.component.html',
  styleUrls: ['./cookie-items.component.css']
})
export class CookieItemsComponent implements OnInit {

  constructor(private cookieItmeService: CookieItmeService) { }
  cookieListItem: CookieListItem[];

  ngOnInit() {
  	this.cookieItmeService.getCookieItems().subscribe(
  		items => {
  			console.log(items);
  			this.cookieListItem = items;
  		}
  		);
  }

}
