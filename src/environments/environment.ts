// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
  	apiKey: "AIzaSyBM4VFqQF-QvBN0jfHzDCO0JZW7ijkyaBQ",
    authDomain: "biotech-test-696d1.firebaseapp.com",
    databaseURL: "https://biotech-test-696d1.firebaseio.com",
    projectId: "biotech-test-696d1",
    storageBucket: "biotech-test-696d1.appspot.com",
    messagingSenderId: "486363288672"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
